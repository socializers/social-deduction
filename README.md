## Setting Up
### Required tools for contributing
- [docker](https://docs.docker.com/get-docker/)
- [docker-compose](https://docs.docker.com/compose/install/)
- [gitlab-runner](https://docs.gitlab.com/runner/)

### Prod

create file `./.key`

```sh
echo "SECRET_KEY_BASE=$(date | base64)" > .key
```

#### Kubernetes

Set namespace

```sh
export NAMESPACE=social-deduction
kubectl create ns $NAMESPACE
```

deploy postgres

```sh
# helm v3
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm install postgres bitnami/postgresql \
  --namespace $NAMESPACE \
  --set "postgresqlDatabase=social-production" \
  --set "postgresqlUsername=social"
```

deploy redis

```sh
helm install redis bitnami/redis \
  --namespace $NAMESPACE \
  --set cluster.enabled=false
```

deploy application

```sh
# configmap
kubectl -n $NAMESPACE apply -f k8s/configmap.yaml

# secrets
# needed for macOS `tr`
export LC_CTYPE=C
export SECRET_STRING=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1 | base64)
sed "s%<SECRET_STRING>%${SECRET_STRING}%g" k8s/secrets.yaml | kubectl -n $NAMESPACE apply -f -

# Deploy
export IMAGE_NAME=registry.gitlab.com/socializers/social-deduction:2020-06-14_082350
sed "s%<IMAGE_NAME>%${IMAGE_NAME}%g" k8s/deploy.yaml | kubectl -n $NAMESPACE apply -f -
```

### Local Dev

Start up

```sh
docker build -t social-deduction ./
docker-compose up -d
docker-compose exec social-deduction bash
```

Shutdown

```sh
docker-compose down
```

### Gitlab CI Dev-ing

run local registry

```sh
docker run -d -p 5000:5000 --restart=always --name registry registry:2
```

build dev image

```sh
docker build -t localhost:5000/social-deduction:$(git rev-parse HEAD) ./
docker push localhost:5000/social-deduction:$(git rev-parse HEAD)
```

run gitlab-ci job

```sh
./bin/gci "Test"
```

### Access postgres

```text
PostgreSQL can be accessed via port 5432 on the following DNS name from within your cluster:

    postgres-postgresql.social-deduction.svc.cluster.local - Read/Write connection

To get the password for "social" run:

    export POSTGRES_PASSWORD=$(kubectl get secret --namespace social-deduction postgres-postgresql -o jsonpath="{.data.postgresql-password}" | base64 --decode)

To connect to your database run the following command:

    kubectl run postgres-postgresql-client --rm --tty -i --restart='Never' --namespace social-deduction --image docker.io/bitnami/postgresql:11.8.0-debian-10-r5 --env="PGPASSWORD=$POSTGRES_PASSWORD" --command -- psql --host postgres-postgresql -U social -d social-production -p 5432



To connect to your database from outside the cluster execute the following commands:

    kubectl port-forward --namespace social-deduction svc/postgres-postgresql 5432:5432 &
    PGPASSWORD="$POSTGRES_PASSWORD" psql --host 127.0.0.1 -U social -d social-production -p 5432
```

## Clean up

Deleting postgres

```sh
helm -n $NAMESPACE delete postgres
kubectl -n $NAMESPACE delete pvc $(kubectl get pvc | awk '/postgres/ {print $1}')
```

Deleting social-deduction

```sh
kubectl -n $NAMESPACE delete deploy/social-deduction
kubectl -n $NAMESPACE delete configmap/social-deduction-config
```