class ChurchillSupporter < Character
  private
  def special_knowledge(characters)
    if churchill = characters.find{|c| c.class == Churchill}
      return [
        "#{churchill.name} is Churchill."
      ]
    end
    []
  end
end
